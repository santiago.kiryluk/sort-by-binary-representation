import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortByBinary {

public static void main(String[] args) {
	
    List<Integer> listOfNumbers = new ArrayList<>();
    listOfNumbers.add(1);
    listOfNumbers.add(15);
    listOfNumbers.add(5);
    listOfNumbers.add(7);
    listOfNumbers.add(3);
      
    System.out.println("The numbers before binary sort: " + listOfNumbers.toString());

    System.out.println("The numbers after binary sort: " + sortByBinaryRepresentation(listOfNumbers).toString());

}

/**
 * return the ArrayList in order by binary representation were the first elemnt 
 * is the number that have more quantity of 1 have in his representation
 * @param  list  is a List<Integer> thats contents all number to sort
 * @return list  a ner List<Integer> with sorted number by binary representation
*/

public static List<Integer> sortByBinaryRepresentation(List<Integer> list) {

    Collections.sort(list, new Comparator<Integer>() {

        @Override
        public int compare(Integer number1, Integer number2) {

            int result = 0;
           
            if (number1 == number2) {
                result = 0;
            } else if (Integer.bitCount(number1) < Integer.bitCount(number2)) {
                                                                         
                result = 1;
            } else if (Integer.bitCount(number1) > Integer.bitCount(number2)) { 
                                                                         
                result = -1;
            } else if (Integer.bitCount(number1) == Integer.bitCount(number2)) { 
                                                                          
                result = (number1 < number2) ? -1 : 1;
            }
            return result;
        }
    });

    return list;
    }

}